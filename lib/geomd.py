#############################################
### FINITE DIFFERENCE METHOD (FDM)        ###
### GEOMETRIC DOMAIN FUNCTION DEFINITIONS ###
### by: OAMEED NOAKOASTEEN                ###
#############################################

import numpy as np

def get_nodes_boundaries_geometry_rectangle(PARAMS):
 
 def get_global_number(I,J,N):
  return J*N+(I+1)
 
 def get_neighbors(NODE,COORDS):
  def get_neighbor_index(NODE,COORDS):   
   try:
    index=COORDS.index(NODE)
   except ValueError:
    index=-1
   return index
  neighbor_right       =[NODE[0]+1,NODE[1]  ]
  neighbor_top         =[NODE[0]  ,NODE[1]+1]
  neighbor_left        =[NODE[0]-1,NODE[1]  ]
  neighbor_bottom      =[NODE[0]  ,NODE[1]-1]
  neighbor_index_right =get_neighbor_index(neighbor_right ,COORDS)
  neighbor_index_top   =get_neighbor_index(neighbor_top   ,COORDS)
  neighbor_index_left  =get_neighbor_index(neighbor_left  ,COORDS)
  neighbor_index_bottom=get_neighbor_index(neighbor_bottom,COORDS)
  return [neighbor_index_right,neighbor_index_top,neighbor_index_left,neighbor_index_bottom]
 
 lx    =PARAMS[0][0]
 ly    =PARAMS[0][1]
 NDx   =PARAMS[0][2]
 
 dx    =lx/NDx
 NDy   =int(ly/dx)
 
 nx    =NDx+1
 ny    =NDy+1
 nodes =[]
 NODES =[]
 
 nodes.append  ([[0   ,0   ],[-1,-1, 2, 3]])
 nodes.append  ([[nx-1,0   ],[ 0,-1,-1, 3]])
 nodes.append  ([[nx-1,ny-1],[ 0, 1,-1,-1]])
 nodes.append  ([[0   ,ny-1],[-1, 1, 2,-1]])
 for  i in range(1,nx-1):
  nodes.append ([[i   ,0   ],[-1,-1,-1, 3]])
 for  j in range(1,ny-1):
  nodes.append ([[nx-1,j   ],[ 0,-1,-1,-1]])
 for  i in range(1,nx-1):
  nodes.append ([[i   ,ny-1],[-1, 1,-1,-1]])
 for  j in range(1,ny-1):
  nodes.append ([[0   ,j   ],[-1,-1, 2,-1]])
 for  i in range(1,nx-1):
  for j in range(1,ny-1):
   nodes.append([[i   ,j   ],[-1,-1,-1,-1]])
 
 nodes_global_numbers=[get_global_number(node[0][0],node[0][1],nx) for node in nodes]
 sorting_indeces     =np.argsort(nodes_global_numbers).tolist()
 nodes               =[nodes[i] for i in sorting_indeces]
 
 coords=[node[0] for node in nodes]
 for  i in range(len(nodes)):
  ngbr =get_neighbors(nodes[i][0],coords)
  NODES.append([ngbr,nodes[i][1]])
 
 return NODES,dx,nx,ny 

# right  wall: 0
# top    wall: 1
# left   wall: 2
# bottom wall: 3
 
# null       : -1
 
# each node element is added with the following convention:
# nodes[*][0]: coordinates: [i,j]
# nodes[*][1]: boundary points of the laplacian stencil [right,top,left,bottom]


