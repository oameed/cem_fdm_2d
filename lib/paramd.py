######################################
### FINITE DIFFERENCE METHOD (FDM) ###
### PARAMETER DEFINITIONS          ###
### by: OAMEED NOAKOASTEEN         ###
######################################


import os
import numpy as np
import argparse

### DEFINE PARSER
parser=argparse.ArgumentParser()

### DEFINE PARAMETERS
parser.add_argument('-v' ,type=str              , help='SPECIFY SIMULATION VERSION'       ,required=True)
parser.add_argument('-pt',type=int              , help='PROBLEM TYPE'                     ,required=True)
parser.add_argument('-lx',type=float,default=1  , help='LENGTH  OF DOMAIN IN X DIRECTION'               )
parser.add_argument('-ly',type=float,default=0.5, help='LENGTH  OF DOMAIN IN Y DIRECTION'               )
parser.add_argument('-n' ,type=int  ,default=100, help='NUMBER  OF DIVISIONS ALONG X-AXIS'              )

### ENABLE FLAGS
args=parser.parse_args()
   
### CONSTRUCT PARAMETER STRUCTURES

PATHS =[os.path.join('..','..','simulations',args.v)]

PARAMS=[[args.lx,args.ly,args.n]]

if args.pt==0:
 PARAMS.append ([['dirichlet',0 ],['dirichlet',0  ],['dirichlet',1],['dirichlet',0 ]])
else:
 if args.pt==1:
  PARAMS.append([['neumann'  ,10],['neumann'  ,-10],['dirichlet',0],['dirichlet',0 ]])

# PARAMS[0][1]: length of the domain along x axis
# PARAMS[0][2]: length of the domain along y axis
# PARAMS[0][3]: number of divisions along the x axis
# PARAMS[1][0]: boundary condition on the right  wall
# PARAMS[1][1]: boundary condition on the top    wall
# PARAMS[1][2]: boundary condition on the left   wall
# PARAMS[1][3]: boundary condition on the bottom wall


