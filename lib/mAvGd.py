#########################################
### FINITE DIFFERENCE METHOD (FDM)    ###
### MATRIX A AND VECTOR G DEFINITIONS ###
### by: OAMEED NOAKOASTEEN            ###
#########################################

import numpy as np

def get_matrix_A_and_vector_G(PARAMS,NODES,DX):
 def get_index_for_neumann(J):
  return [2,3,0,1][J]
 size=len(NODES)
 A   =np.zeros((size,size))
 G   =np.zeros((size,1   ))
 for i in range(size):
  A[i,i]              +=-4
  neighbors            =NODES[i][0]
  boundaries           =NODES[i][1]
  for j in range(4):
   if not neighbors[j]==-1:
    A[i,neighbors[j]] +=1
  for j in range(4):
   if     neighbors[j]==-1:
    boundary_tag       =boundaries[j]
    boundary_type,value=PARAMS[1][boundary_tag]
    if  boundary_type=='dirichlet':
     G[i]+=value
    else:
     if boundary_type=='neumann'  :
      index            =get_index_for_neumann(j)
      A[i,index]      +=1
      G[i]            +=2*DX*value
      
 A= (1/np.power(DX,2))*A
 G=-(1/np.power(DX,2))*G
 return A,G


