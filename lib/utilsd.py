######################################
### FINITE DIFFERENCE METHOD (FDM) ###
### UTILITY FUNCTION DEFINITIONS   ###
### by: OAMEED NOAKOASTEEN         ###
######################################

import h5py

def wHDF(FILENAME,DIRNAMES,DATA):
 fobj=h5py.File(FILENAME,'w')
 for i in range(len(DIRNAMES)):
  fobj.create_dataset(DIRNAMES[i],data=DATA[i])
 fobj.close()


