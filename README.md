# Finite Difference Method (FDM) for 2D Problems

This project demonstrates application of the FDM to the follwing problem:
* Electrostatic potential distribution in source-less region  
  _The all-Dirichlet BCs version of this problem is presented in Example 4-7 of Ref [1]_  


## Reference
[1]. 1989. Cheng. Field and Wave Electromagnetics. 2nd Edition

## Code Statistics
<pre>
github.com/AlDanial/cloc v 1.90  T=0.01 s (648.8 files/s, 26386.1 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
Python                           5             44             44            118
MATLAB                           1             11              8             56
Markdown                         1             13              0             38
Bourne Shell                     2             12              0             22
-------------------------------------------------------------------------------
SUM:                             9             80             52            234
-------------------------------------------------------------------------------
</pre>

## How to Run

To run simulations:  
   1. `cd` to the main project directory
   2. `./run/sh/run_v01.sh`


## Simulation 'v01': Dirichlet-Dirichlet-Dirichlet-Dirichlet Boundary Conditions

|     |
|:---:|
**Numerical Solution**
![][fig1]
**Analytic Solution**
![][fig2]

[fig1]:simulations/v01/data.png
[fig2]:simulations/v01/data_analytic.png
## Simulation 'v02': Neumann-Neumann-Dirichlet-Dirichlet Boundary Conditions

|     |
|:---:|
![][fig3]

[fig3]:simulations/v02/data.png

