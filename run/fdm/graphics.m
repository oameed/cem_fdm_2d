%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% FINITE DIFFERENCE METHOD (FDM) %%%
%%% GRAPHICS                       %%%
%%% by: OAMEED NOAKOASTEEN         %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function graphics(V,DIR,ANALYTIC)
close all
clc

PATH         =fullfile('..','..','simulations',V)                          ;
filename     =fullfile(PATH,'data')                                        ;

voltage      =h5read(fullfile(PATH,strcat('data','.h5')),strcat('/',DIR))  ;
voltage      =permute(voltage,[2,1])                                       ;
[v_g_x,v_g_y]=gradient(voltage)                                            ;
v_g_x        =-v_g_x                                                       ;
v_g_y        =-v_g_y                                                       ;
plotter(voltage,v_g_x,v_g_y,filename)

if ANALYTIC{1}
    Lx           =ANALYTIC{2}{1}                 ;
    Ly           =ANALYTIC{2}{2}                 ;
    v0           =ANALYTIC{2}{3}                 ;
    filename     =strcat(filename,'_','analytic');
    voltage      =get_analytic_voltage(Lx,Ly,v0) ;
    [v_g_x,v_g_y]=gradient(voltage)              ;
    v_g_x        =-v_g_x                         ;
    v_g_y        =-v_g_y                         ;
    plotter(voltage,v_g_x,v_g_y,filename)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% FUNCTION DEFINITIONS %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function vltg=get_analytic_voltage(A,B,V0)
        resolution=50;
        [x,y]     =meshgrid(linspace(0,A,resolution      ),...
                            linspace(0,B,resolution*(B/A))    );
        vltg      =zeros(size(x,1),size(x,2))           ;
        for N=1:50
            n   =2*N-1                                                               ;
            vltg=vltg+(1/(n*sinh(n*pi*A/B))).*sinh((n*pi/B).*(A-x)).*sin((n*pi/B).*y);
        end
        vltg    =(4*V0/pi).*vltg                                                   ;
    end

    function plotter(VOLTAGE,VGX,VGY,FILENAME)
        fig_size     =size(VOLTAGE,1)/size(VOLTAGE,2);
        fig          =figure                         ;
        fig.Renderer ='painters'                     ;
        position     =fig.Position                   ;
        set(fig,'Position',[position(1),position(2),1.5*position(3),1.5*fig_size*position(3)])
        
        Ln            =sqrt(VGX.^2 + VGY.^2)                ; % CLAMP LARGE MAGNITUDES OF LARGE VECTORS 
        Maxlen        =0.125*max(max(Ln))                   ;
        VGX(Ln>Maxlen)=VGX(Ln>Maxlen)./Ln(Ln>Maxlen).*Maxlen;
        VGY(Ln>Maxlen)=VGY(Ln>Maxlen)./Ln(Ln>Maxlen).*Maxlen;
        
        contour(VOLTAGE,50,'linewidth',1.5)
        hold
        quiver(VGX,VGY,1.5)
        colormap jet
        shading  interp
        axis     equal
        view(2)
        set(gca,'xticklabel',[]   ,...
                'yticklabel',[]   ,...
                'xtick'     ,[]   ,...
                'ytick'     ,[]       )
        colorbar
        saveas(gca,strcat(FILENAME,'.png'),'png')
        saveas(gca,strcat(FILENAME,'.svg'),'svg')
        close all
    end

end

