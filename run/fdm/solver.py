######################################
### FINITE DIFFERENCE METHOD (FDM) ###
### SOLVER                         ###
### by: OAMEED NOAKOASTEEN         ###
######################################

import os
import sys
sys.path.append(os.path.join(sys.path[0],'..','..','lib'))
import numpy    as np

from paramd import PATHS,PARAMS
from utilsd import wHDF
from geomd  import get_nodes_boundaries_geometry_rectangle
from mAvGd  import get_matrix_A_and_vector_G

nodes,dx,Nx,Ny=get_nodes_boundaries_geometry_rectangle(PARAMS         )
A,G           =get_matrix_A_and_vector_G              (PARAMS,nodes,dx)

voltage       =np.linalg.solve(A,G)
voltage       =np.reshape(voltage,[Ny,Nx])

wHDF(os.path.join(PATHS[0],'data'+'.h5'),
     ['voltage']                        ,
     [ voltage ]                         )


