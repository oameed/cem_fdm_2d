#! /bin/bash

source activate pylnx

cd      run/fdm

echo ' CREATING PROJECT DIRECTORY '                            'v02'
rm     -rf                                    ../../simulations/v02
tar    -xzf  ../../simulations/v00.tar.gz -C  ../../simulations
mv           ../../simulations/v00            ../../simulations/v02

echo ' RUNNING FINITE DIFFERENCE SIMULATION'
python solver.py -v v02 -pt 1 -n 100

echo ' RUNNING VISUALIZATIONS '
#matlab -nodisplay -nosplash -nodesktop -r "graphics('v02','voltage',{false,{}});exit;"
matlab -batch "graphics('v02','voltage',{false,{}})" -noFigureWindows

