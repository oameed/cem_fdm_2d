#! /bin/bash

source activate pylnx

cd      run/fdm

echo ' CREATING PROJECT DIRECTORY '                            'v01'
rm     -rf                                    ../../simulations/v01
tar    -xzf  ../../simulations/v00.tar.gz -C  ../../simulations
mv           ../../simulations/v00            ../../simulations/v01

echo ' RUNNING FINITE DIFFERENCE SIMULATION'
python solver.py -v v01 -pt 0 -n 50

echo ' RUNNING VISUALIZATIONS '
#matlab -nodisplay -nosplash -nodesktop -r "graphics('v01','voltage',{true,{1,0.5,1}});exit;"
matlab -batch "graphics('v01','voltage',{true,{1,0.5,1}})" -noFigureWindows

